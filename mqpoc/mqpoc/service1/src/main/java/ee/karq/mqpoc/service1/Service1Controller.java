package ee.karq.mqpoc.service1;

import com.google.protobuf.Any;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import ee.karq.mqpoc.common.activemq.LocalGateway;
import ee.karq.mqpoc.common.proto.Corpus;
import ee.karq.mqpoc.common.proto.GetNameRequest;
import ee.karq.mqpoc.common.proto.base.BaseMessage;
import ee.karq.mqpoc.common.proto.config.ProtoBufMessageRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.UUID;

@Controller
public class Service1Controller {

    private static Logger log = LoggerFactory.getLogger(Service1Controller.class);

    public static final String NAME_QUEUE = "name-queue";

    @Autowired
    private LocalGateway localGateway;

    public static void main(String[] args) throws InvalidProtocolBufferException {
        GetNameRequest request = GetNameRequest.newBuilder()
                .setCorpus(Corpus.UNIVERSAL)
                .setId(1)
                .setSomething(4)
                .build();
        BaseMessage baseMessage = BaseMessage
                .newBuilder()
                .setCorrelationId(UUID.randomUUID().toString())
                .setMessage(Any.pack(request)).build();
        String message = JsonFormat
                .printer()
                .usingTypeRegistry(ProtoBufMessageRegistry.getInstance().getRegistry())
                .print(baseMessage);
        System.out.println(message);
    }

    @RequestMapping("/name")
    public @ResponseBody Object name() throws IOException {
        GetNameRequest request = GetNameRequest.newBuilder()
                .setCorpus(Corpus.UNIVERSAL)
                .setId(1)
                .setSomething(4)
                .build();
        BaseMessage baseMessage = BaseMessage
                .newBuilder()
                .setCorrelationId(UUID.randomUUID().toString())
                .setMessage(Any.pack(request)).build();
        BaseMessage gatewayResponse = localGateway.sendReceive(baseMessage, NAME_QUEUE);
      /*  if(gatewayResponse != null) {
            if (gatewayResponse.getErrorText() != null) {
                return gatewayResponse.getErrorText();
            }else{
                NameResponse nameResponse = (NameResponse)gatewayResponse;
                return nameResponse.getName();
            }
        } */
        log.info("viga");
        return "viga";
    }

 /*   @RequestMapping("/age")
    public @ResponseBody Object age() throws IOException {
        AgeRequest request = new AgeRequest();
        request.setId(1);
        log.info("Send request:" + request.getCorrelationId());
        GatewayResponse gatewayResponse = localGateway.sendReceive(request, NAME_QUEUE);
        if(gatewayResponse != null) {
            log.info("Response received:" + gatewayResponse.getCorrelationId());
            if (gatewayResponse.getErrorText() != null) {
                return gatewayResponse.getErrorText();
            }
            AgeResponse ageResponse = AgeResponse.class.cast(gatewayResponse);
            return ageResponse.getAge();
        }
        log.error("viga");
        return "viga";
    } */
}
