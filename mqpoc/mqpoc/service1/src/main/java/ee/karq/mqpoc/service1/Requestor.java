package ee.karq.mqpoc.service1;

import ee.karq.mqpoc.common.activemq.api.service2.NameRequest;
import ee.karq.mqpoc.common.activemq.api.service2.NameResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.UUID;

@Component
public class Requestor {
 
    private final JmsTemplate jmsTemplate;
 
    @Autowired
    public Requestor( JmsTemplate jmsTemplate ) {

        this.jmsTemplate = jmsTemplate;
    }
 
    public NameResponse request(final NameRequest request, String queue ) throws IOException {
        final String correlationId = UUID.randomUUID().toString();
        jmsTemplate.convertAndSend( queue+".request", request, new CorrelationIdPostProcessor( correlationId ) );
        return (NameResponse) jmsTemplate.receiveSelectedAndConvert( queue+".response", "JMSCorrelationID='" + correlationId + "'" );
    }
}