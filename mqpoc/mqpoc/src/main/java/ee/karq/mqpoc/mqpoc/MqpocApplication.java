package ee.karq.mqpoc.mqpoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MqpocApplication {

	public static void main(String[] args) {
		SpringApplication.run(MqpocApplication.class, args);
	}
}
