/*
 * Copyright 2002-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ee.karq.mqpoc.mqpoc;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import com.fasterxml.jackson.databind.ObjectWriter;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageOrBuilder;
import com.google.protobuf.util.JsonFormat;
import ee.karq.mqpoc.common.proto.base.BaseMessage;
import ee.karq.mqpoc.common.proto.config.ProtoBufMessageRegistry;
import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.jms.support.converter.SmartMessageConverter;
import org.springframework.util.Assert;


public class ProtoBufMessageConverter implements SmartMessageConverter {

	/**
	 * The default encoding used for writing to text messages: UTF-8.
	 */
	public static final String DEFAULT_ENCODING = "UTF-8";

	private MessageType targetType = MessageType.BYTES;

	private String encoding = DEFAULT_ENCODING;


	public ProtoBufMessageConverter() {
	}


	public void setTargetType(MessageType targetType) {
		Assert.notNull(targetType, "MessageType must not be null");
		this.targetType = targetType;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}



	@Override
	public Message toMessage(Object object, Session session) throws JMSException, MessageConversionException {
		Message message = null;
		try {
			switch (this.targetType) {
				case TEXT:
					message = mapToTextMessage(object, session);
					break;
				case BYTES:
					message = mapToBytesMessage(object, session);
					break;
			}
		}
		catch (IOException ex) {
			throw new MessageConversionException("Could not map JSON object [" + object + "]", ex);
		}
		return message;
	}

	@Override
	public Object fromMessage(Message message) throws JMSException, MessageConversionException {
		if(message instanceof TextMessage) {
			TextMessage textMessage = (TextMessage)message;
			BaseMessage.Builder baseMessage = BaseMessage.newBuilder();
			try {
				JsonFormat
						.parser()
						.ignoringUnknownFields()
						.usingTypeRegistry(ProtoBufMessageRegistry.getInstance().getRegistry())
						.merge(textMessage.getText(), baseMessage);
			} catch (InvalidProtocolBufferException e) {
				e.printStackTrace();
			}
			return baseMessage.build();
		}
		return null;
	}


	protected TextMessage mapToTextMessage(Object object, Session session)
			throws JMSException, IOException {

		String message = JsonFormat
				.printer()
				.usingTypeRegistry(ProtoBufMessageRegistry.getInstance().getRegistry())
				.print((MessageOrBuilder) object);
		return session.createTextMessage(message);
	}

	@Deprecated
	protected BytesMessage mapToBytesMessage(Object object, Session session)
			throws JMSException, IOException {

		return mapToBytesMessage(object, session, null);
	}


	protected BytesMessage mapToBytesMessage(Object object, Session session, ObjectWriter objectWriter)
			throws JMSException, IOException {

		ByteArrayOutputStream bos = new ByteArrayOutputStream(1024);
		OutputStreamWriter writer = new OutputStreamWriter(bos, this.encoding);
		objectWriter.writeValue(writer, object);

		BytesMessage message = session.createBytesMessage();
		message.writeBytes(bos.toByteArray());
		return message;
	}

	@Override
	public Message toMessage(Object object, Session session, Object conversionHint) throws JMSException, MessageConversionException {
		return null;
	}
}
