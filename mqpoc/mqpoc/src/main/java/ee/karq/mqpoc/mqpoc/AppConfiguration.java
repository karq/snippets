package ee.karq.mqpoc.mqpoc;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"ee.karq.mqpoc.service1","ee.karq.mqpoc.service2","ee.karq.mqpoc.common"})
public class AppConfiguration {
}
