// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: GetName.proto

package ee.karq.mqpoc.common.proto;

public interface GetNameRequestOrBuilder extends
    // @@protoc_insertion_point(interface_extends:GetNameRequest)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   *this a id for user
   * </pre>
   *
   * <code>int64 id = 1;</code>
   */
  long getId();

  /**
   * <code>repeated int32 requiredField = 2;</code>
   */
  java.util.List<java.lang.Integer> getRequiredFieldList();
  /**
   * <code>repeated int32 requiredField = 2;</code>
   */
  int getRequiredFieldCount();
  /**
   * <code>repeated int32 requiredField = 2;</code>
   */
  int getRequiredField(int index);

  /**
   * <code>.Corpus corpus = 3;</code>
   */
  int getCorpusValue();
  /**
   * <code>.Corpus corpus = 3;</code>
   */
  ee.karq.mqpoc.common.proto.Corpus getCorpus();

  /**
   * <code>fixed64 something = 4;</code>
   */
  long getSomething();

  /**
   * <code>map&lt;string, string&gt; mappiga = 5;</code>
   */
  int getMappigaCount();
  /**
   * <code>map&lt;string, string&gt; mappiga = 5;</code>
   */
  boolean containsMappiga(
      java.lang.String key);
  /**
   * Use {@link #getMappigaMap()} instead.
   */
  @java.lang.Deprecated
  java.util.Map<java.lang.String, java.lang.String>
  getMappiga();
  /**
   * <code>map&lt;string, string&gt; mappiga = 5;</code>
   */
  java.util.Map<java.lang.String, java.lang.String>
  getMappigaMap();
  /**
   * <code>map&lt;string, string&gt; mappiga = 5;</code>
   */

  java.lang.String getMappigaOrDefault(
      java.lang.String key,
      java.lang.String defaultValue);
  /**
   * <code>map&lt;string, string&gt; mappiga = 5;</code>
   */

  java.lang.String getMappigaOrThrow(
      java.lang.String key);
}
