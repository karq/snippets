package ee.karq.mqpoc.common.activemq.api.service2;

import ee.karq.mqpoc.common.activemq.api.GatewayRequest;
import lombok.Data;

@Data
public class AgeRequest extends GatewayRequest {

    private int id;
}
