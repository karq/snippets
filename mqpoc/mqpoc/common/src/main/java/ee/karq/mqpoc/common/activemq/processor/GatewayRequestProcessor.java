package ee.karq.mqpoc.common.activemq.processor;

import ee.karq.mqpoc.common.activemq.api.GatewayRequest;
import ee.karq.mqpoc.common.activemq.api.GatewayResponse;

public interface GatewayRequestProcessor<T extends GatewayRequest, K extends GatewayResponse> {

    K process(T request);
}
