package ee.karq.mqpoc.common.proto.config;

import com.google.protobuf.util.JsonFormat;
import ee.karq.mqpoc.common.proto.GetNameRequest;
import ee.karq.mqpoc.common.proto.GetNameResponse;

public class ProtoBufMessageRegistry {

    private JsonFormat.TypeRegistry registry;

    private static ProtoBufMessageRegistry protoBufMessageRegistry;

    private ProtoBufMessageRegistry(){
        registry = JsonFormat.TypeRegistry.newBuilder()
                .add(GetNameRequest.getDescriptor())
                .add(GetNameResponse.getDescriptor())
                .build();
    }

    public static ProtoBufMessageRegistry getInstance() {
        if(protoBufMessageRegistry == null){
            protoBufMessageRegistry = new ProtoBufMessageRegistry();
        }
        return protoBufMessageRegistry;
    }

    public JsonFormat.TypeRegistry getRegistry() {
        return registry;
    }
}
