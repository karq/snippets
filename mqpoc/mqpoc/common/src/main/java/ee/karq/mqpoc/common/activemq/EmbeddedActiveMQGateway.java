package ee.karq.mqpoc.common.activemq;

import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import ee.karq.mqpoc.common.activemq.api.GatewayRequest;
import ee.karq.mqpoc.common.activemq.api.GatewayResponse;
import ee.karq.mqpoc.common.proto.base.BaseMessage;
import ee.karq.mqpoc.common.proto.config.ProtoBufMessageRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.core.MessagePostProcessor;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;
import java.util.UUID;

@Component
public class EmbeddedActiveMQGateway implements LocalGateway {

    private static Logger log = LoggerFactory.getLogger(EmbeddedActiveMQGateway.class);

    private final JmsTemplate jmsTemplate;

    @Autowired
    public EmbeddedActiveMQGateway(JmsTemplate jmsTemplate){
        this.jmsTemplate = jmsTemplate;
    }

    @Override
    public BaseMessage sendReceive(BaseMessage request, String queue){
        //log.info("Request sent:" + request.getCorrelationId());
        Message message = jmsTemplate.sendAndReceive(queue+".request",new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                try {
                    String message = JsonFormat
                            .printer()
                            .usingTypeRegistry(ProtoBufMessageRegistry.getInstance().getRegistry())
                            .print(request);
                    TextMessage textMessage = session.createTextMessage(message);
                    textMessage.setJMSCorrelationID(request.getCorrelationId());
                    return textMessage;
                } catch (InvalidProtocolBufferException e) {
                    e.printStackTrace();
                }
                return null;
            }
        });
        return null;

        //if protobufMessageConverter is used
       /* jmsTemplate.convertAndSend(queue+".request",request, new MessagePostProcessor(){

            @Override
            public Message postProcessMessage(Message message) throws JMSException {
                message.setJMSCorrelationID(request.getCorrelationId());
                return message;
            }
        });
        Object response = jmsTemplate.receiveSelectedAndConvert(queue + ".response", "JMSCorrelationID='" + request.getCorrelationId() + "'");
        //log.info("Response recieved:" + response.getCorrelationId());
        return (BaseMessage) response; */
    }
}
