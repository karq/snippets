package ee.karq.mqpoc.common.activemq.api;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GatewayResponse {

    private String correlationId;
    private String errorText;
}
