package ee.karq.mqpoc.common.activemq.api.service2;

import ee.karq.mqpoc.common.activemq.api.GatewayResponse;
import lombok.Data;

@Data
public class AgeResponse extends GatewayResponse {

    private int age;
}
