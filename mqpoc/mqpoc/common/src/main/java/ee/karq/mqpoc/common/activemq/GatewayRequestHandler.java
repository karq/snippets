package ee.karq.mqpoc.common.activemq;

import ee.karq.mqpoc.common.activemq.api.GatewayRequest;
import ee.karq.mqpoc.common.activemq.api.GatewayResponse;
import ee.karq.mqpoc.common.activemq.processor.GatewayRequestProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class GatewayRequestHandler {


    @Autowired
    private ApplicationContext context;

    public <K extends GatewayResponse, T extends GatewayRequest, S extends GatewayRequestProcessor> K process(GatewayRequest request, Map<Class<T>, Class<S>> processors) {
        Class processorClass = processors.get(request.getClass());
        if(processorClass == null){
            return createErrorMessage("Request not supported");
        }
        GatewayRequestProcessor processor = (GatewayRequestProcessor) context.getBean(processorClass);
        if(processor != null){
            return (K) processor.process(request);
        }else{
            return createErrorMessage("Request not implemented");
        }

    }

    private <K extends GatewayResponse> K createErrorMessage(String message) {
        GatewayResponse response = new GatewayResponse();
        response.setErrorText(message);
        return (K) response;
    }
}
