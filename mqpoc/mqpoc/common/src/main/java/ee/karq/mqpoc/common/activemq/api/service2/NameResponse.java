package ee.karq.mqpoc.common.activemq.api.service2;

import ee.karq.mqpoc.common.activemq.api.GatewayResponse;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NameResponse extends GatewayResponse {

    private String name;
}
