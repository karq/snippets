package ee.karq.mqpoc.common.activemq.api;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class GatewayRequest {

    private final String correlationId;

    protected GatewayRequest() {
        this.correlationId = UUID.randomUUID().toString();
    }
}
