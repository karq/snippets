package ee.karq.mqpoc.common.protobuf;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageOrBuilder;
import com.google.protobuf.util.JsonFormat;
import ee.karq.mqpoc.common.proto.Corpus;
import ee.karq.mqpoc.common.proto.GetNameRequest;
import ee.karq.mqpoc.common.proto.GetNameResponse;

public class ProtoTest {


    public static void main(String[] args) throws InvalidProtocolBufferException {

        GetNameRequest req = GetNameRequest.newBuilder()
                .setId(1)
                .build();

        GetNameRequest build1 = req.newBuilderForType().setCorpus(Corpus.LOCAL).build();
        System.out.println(build1);
        String print = JsonFormat.printer().print(req);
        System.out.println(print);


        String response = "{ \"name\": \"Mari\", \"age\" :12 }";
       GetNameResponse.Builder getNameResponse = GetNameResponse.newBuilder();
        JsonFormat.parser().ignoringUnknownFields().merge(response,getNameResponse);
        GetNameResponse build = getNameResponse.build();
        System.out.println(build);

        String req2 = "{ \"id\": \"fdafs\", \"requiredField\" :[13] }";
        GetNameRequest.Builder req22 = GetNameRequest.newBuilder();
        JsonFormat.parser().ignoringUnknownFields().merge(req2,req22);
        GetNameRequest build2 = req22.build();
        System.out.println(build2);

    }
}
