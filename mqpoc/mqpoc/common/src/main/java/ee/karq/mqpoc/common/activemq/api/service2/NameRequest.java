package ee.karq.mqpoc.common.activemq.api.service2;

import ee.karq.mqpoc.common.activemq.api.GatewayRequest;
import lombok.Data;

@Data
public class NameRequest extends GatewayRequest {

    private int id;


}
