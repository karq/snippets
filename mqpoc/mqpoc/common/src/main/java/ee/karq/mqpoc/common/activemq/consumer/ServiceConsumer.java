package ee.karq.mqpoc.common.activemq.consumer;

import ee.karq.mqpoc.common.activemq.api.GatewayRequest;
import ee.karq.mqpoc.common.activemq.api.GatewayResponse;
import ee.karq.mqpoc.common.activemq.api.service2.NameRequest;
import ee.karq.mqpoc.common.activemq.api.service2.NameResponse;
import ee.karq.mqpoc.common.activemq.processor.GatewayRequestProcessor;
import ee.karq.mqpoc.common.proto.base.BaseMessage;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;

import javax.jms.Session;
import java.util.Map;

public interface ServiceConsumer {

     String receiveMessage(@Payload String request) throws Exception;

     <K extends GatewayRequest, T extends GatewayRequestProcessor> Map<Class<K>, Class<T>> getProcessors();
}
