package ee.karq.mqpoc.common.activemq;

import com.google.protobuf.GeneratedMessageV3;
import ee.karq.mqpoc.common.activemq.api.GatewayRequest;
import ee.karq.mqpoc.common.activemq.api.GatewayResponse;
import ee.karq.mqpoc.common.proto.base.BaseMessage;

public interface LocalGateway {

    BaseMessage sendReceive(BaseMessage request, String queue);
}
