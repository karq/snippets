package ee.karq.mqpoc.service2;

import com.google.protobuf.Any;
import com.google.protobuf.util.JsonFormat;
import ee.karq.mqpoc.common.activemq.GatewayRequestHandler;
import ee.karq.mqpoc.common.activemq.api.GatewayRequest;
import ee.karq.mqpoc.common.activemq.api.GatewayResponse;
import ee.karq.mqpoc.common.activemq.api.service2.NameRequest;
import ee.karq.mqpoc.common.activemq.consumer.ServiceConsumer;
import ee.karq.mqpoc.common.activemq.processor.GatewayRequestProcessor;
import ee.karq.mqpoc.common.proto.GetNameRequest;
import ee.karq.mqpoc.common.proto.GetNameResponse;
import ee.karq.mqpoc.common.proto.base.BaseMessage;
import ee.karq.mqpoc.common.proto.config.ProtoBufMessageRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

import javax.jms.TextMessage;
import java.util.HashMap;
import java.util.Map;

@Component
public class Service2Consumer implements ServiceConsumer {

    public static final String NAME_QUEUE = "name-queue";

    private static Logger log = LoggerFactory.getLogger(Service2Consumer.class);

    @Autowired
    private GatewayRequestHandler gatewayRequestHandler;


   /* public NameResponse receiveMessage(@Payload NameRequest order,
                               @Headers MessageHeaders headers,
                               Message message, Session session) {

    }*/

    @Override
    @JmsListener(destination = NAME_QUEUE + ".request")
    @SendTo(NAME_QUEUE +".response")
    public String receiveMessage(String request) throws Exception {
          //  log.info("Request received:" + request);
        BaseMessage.Builder builder = BaseMessage.newBuilder();
        JsonFormat.parser().usingTypeRegistry(ProtoBufMessageRegistry.getInstance().getRegistry())
                .ignoringUnknownFields()
                .merge(request,builder);
        BaseMessage build = builder.build();
        if(build.getMessage().is(GetNameRequest.class)){
                log.info("on nii tõesti");
                GetNameRequest unpack = build.getMessage().unpack(GetNameRequest.class);
                log.info("" + unpack.getId());
            }

        GetNameResponse getNameResponse = GetNameResponse.newBuilder().setName("uuno").build();
            BaseMessage baseMessage = BaseMessage.newBuilder()
                    .setMessage(Any.pack(getNameResponse))
                    .build();
          //  GatewayResponse response = gatewayRequestHandler.process(request, getProcessors());
          //  log.info("Response sent:" + response.getCorrelationId());
          //  return (K) response;
        return JsonFormat.printer().usingTypeRegistry(ProtoBufMessageRegistry.getInstance().getRegistry())
                .print(baseMessage);
    }

    @Override
    public <K extends GatewayRequest, T extends GatewayRequestProcessor> Map<Class<K>, Class<T>> getProcessors() {
        Map<Class<K>, Class<T>> processors = new HashMap<>();
        processors.put((Class<K>)NameRequest.class, (Class<T>)NameRequestProcessor.class);

        return processors;
    }
}
