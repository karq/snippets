package ee.karq.mqpoc.service2;

import ee.karq.mqpoc.common.activemq.api.service2.NameRequest;
import ee.karq.mqpoc.common.activemq.api.service2.NameResponse;
import ee.karq.mqpoc.common.activemq.processor.GatewayRequestProcessor;
import org.springframework.stereotype.Component;

@Component
public class NameRequestProcessor implements GatewayRequestProcessor<NameRequest, NameResponse>{

    @Override
    public NameResponse process(NameRequest request) {
        NameResponse nameResponse = new NameResponse();
        nameResponse.setCorrelationId(request.getCorrelationId());
        nameResponse.setName("Mari");
        return nameResponse;
    }
}
