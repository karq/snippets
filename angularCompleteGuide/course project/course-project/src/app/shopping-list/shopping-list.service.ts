import {EventEmitter, Injectable} from '@angular/core';
import {Ingredient} from "../shared/ingredient.model";

@Injectable()
export class ShoppingListService {

  ingredientsChanged = new EventEmitter<Ingredient[]>();

 private ingredients :Ingredient[] = [
    new Ingredient('apples',5),
    new Ingredient('tomatos',10),
  ];

  constructor() { }

  getIngredients(){
    return this.ingredients.slice();
  }

  addIgredients(ingredient : Ingredient){
    this.ingredients.push(ingredient);
    this.ingredientsChanged.emit(this.ingredients.slice());
  }

}
