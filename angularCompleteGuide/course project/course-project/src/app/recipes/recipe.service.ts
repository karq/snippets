import {EventEmitter, Injectable} from '@angular/core';
import {Recipe} from "./recipe.model";
import {Ingredient} from "../shared/ingredient.model";

@Injectable()
export class RecipeService {

  recipeSelected = new EventEmitter<Recipe>();

  private recipes: Recipe[] = [
    new Recipe(
      'testRecipe',
      'test description',
      'https://static01.nyt.com/images/2015/08/14/dining/14ROASTEDSALMON/14ROASTEDSALMON-articleLarge.jpg',
      [
        new Ingredient('Meat',1),
        new Ingredient('Potato', 5)
      ]
    ),
    new Recipe(
      'testRecipe2',
      'test description2',
      'https://static01.nyt.com/images/2015/08/14/dining/14ROASTEDSALMON/14ROASTEDSALMON-articleLarge.jpg',
      [
        new Ingredient('bread',2),
        new Ingredient('meat', 1)
      ])

  ];

  constructor() { }

  geetRecipes() {
    return this.recipes.slice();
  }

}
