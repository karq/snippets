package com.karq.dynamicrefresh.dynamicrefresh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DynamicRefreshApplication {

	public static void main(String[] args) {
		SpringApplication.run(DynamicRefreshApplication.class, args);
	}
}
