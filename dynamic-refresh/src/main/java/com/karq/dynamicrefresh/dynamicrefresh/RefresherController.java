package com.karq.dynamicrefresh.dynamicrefresh;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RefresherController {

    @Autowired
    private Environment env;

    @RequestMapping("/test")
    public String greeting() throws InterruptedException {
        int i = 0;
        boolean starter = true;
        while(starter){
            String testikas = env.getProperty("testikas");
            System.out.println(i + " " +  testikas);
            i++;
            Thread.sleep(2000);
        }

        return "done";
    }
}
