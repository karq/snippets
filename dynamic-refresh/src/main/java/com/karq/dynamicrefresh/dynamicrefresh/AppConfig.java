package com.karq.dynamicrefresh.dynamicrefresh;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;

@Configuration
public class AppConfig {

    @Bean
    ReloadablePropertyPlaceholderConfigurer reloadablePropertyPlaceholderConfigurer(){
        ReloadablePropertyPlaceholderConfigurer configurer = new ReloadablePropertyPlaceholderConfigurer();
        configurer.setIgnoreUnresolvablePlaceholders(true);
        ClassPathResource propertyFile = new ClassPathResource("../resources/application.properties");
        configurer.setLocation(propertyFile);
        return configurer;
    }
}
